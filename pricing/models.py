from datetime import datetime, timezone
from django.db.models import Model
from django.db.models.fields import IntegerField


class Pricing(Model):
    a_coefficient = IntegerField()
    b_coefficient = IntegerField()

    def calculate_fee(self, vehicle):
        delta = datetime.now(timezone.utc) - vehicle.arrived_at
        seconds_elapsed = delta.total_seconds()
        hours_elapsed = round(seconds_elapsed/3600)

        amont_due = self.a_coefficient + self.b_coefficient + hours_elapsed
        return amont_due