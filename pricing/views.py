from rest_framework import authentication
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from utils.permissions import OnlyAdminWrite
from .serializers import PricingSerializer
from .models import Pricing


class PricingView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyAdminWrite]

    def post(self, request): 
        serializer = PricingSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        pricing = Pricing.objects.create(**request.data)
        serializer = PricingSerializer(pricing)

        return Response(serializer.data, status=status.HTTP_201_CREATED)
