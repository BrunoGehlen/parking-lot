from rest_framework import serializers


class CredentialSerializer(serializers.Serializer):
    password = serializers.CharField()
    username = serializers.CharField()


class UserSerializer(serializers.Serializer):
    id  = serializers.IntegerField(read_only=True)
    is_staff = serializers.BooleanField()
    is_superuser = serializers.BooleanField()
    username = serializers.CharField()