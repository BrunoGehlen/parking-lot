from django.urls import path
from .views import LoginView, AccountView

urlpatterns = [
    path('login/', LoginView.as_view()),
    path('accounts/', AccountView.as_view())
]