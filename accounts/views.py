from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from .serializers import CredentialSerializer, UserSerializer
from django.contrib.auth.models import User
from rest_framework import serializers, status
from django.contrib.auth import authenticate


class AccountView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

        find_user = User.objects.filter(username=request.data['username']).exists()

        if find_user:
            return Response({"msg": "user already exists"}, status=status.HTTP_409_CONFLICT)

        user = User.objects.create_user(**request.data)
        serializer = UserSerializer(user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class LoginView(APIView):
    def post(self, request):
        serializer = CredentialSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializers.errors, status.HTTP_400_BAD_REQUEST)

        user = authenticate(username=request.data['username'], password=request.data['password'])

        if user is not None:
            token = Token.objects.get_or_create(user=user)[0]
            return Response({'token': token.key})
        
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
