from django.db.models import Model
from django.db.models.deletion import CASCADE
from django.db.models.fields import CharField, DateTimeField, IntegerField
from django.db.models.fields.related import OneToOneField
from levels.models import Space, SpaceType


class Vehicle(Model):
    license_plate = CharField(max_length=255)
    vehicle_type = CharField(max_length=255, choices=SpaceType.choices)
    arrived_at = DateTimeField(auto_now=True)
    paid_at = DateTimeField(null=True)
    amount_paid = IntegerField(null=True)
    space = OneToOneField(Space, on_delete=CASCADE, null=True)
