from levels.models import Level

def has_space_type(level, vehicle_type):
    availabe_spaces = level.space_set.filter(
        variety=vehicle_type, vehicle=None
    )
    if availabe_spaces:
        return availabe_spaces
    else: 
        return None

def find_space_for_vehicle_type(vehicle_type):
    levels_by_priority = Level.objects.order_by('fill_priority')
    for level in levels_by_priority:
        space = has_space_type(level, vehicle_type)
        if space:
            return space
        else:
            continue
    
    return None