from levels.models import Space, SpaceType
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import VehicleSerializer
from rest_framework import serializers, status
from .models import Vehicle
from pricing.models import Pricing
from datetime import datetime, time, timezone
from django.shortcuts import get_object_or_404
from .services  import find_space_for_vehicle_type

class VehicleEntryView(APIView):
    def post(self, request):
        if not Pricing.objects.last():
            return Response({'error': 'no pricing available'}, status=status.HTTP_404_NOT_FOUND)
        serializer = VehicleSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        if request.data['vehicle_type'] not in [k for k, v in SpaceType.choices]:
            return Response({'message': 'invalid vehicle type'})

        space = find_space_for_vehicle_type(request.data['vehicle_type'])

        if space:
            vehicle= Vehicle.objects.create(**request.data, space=space[0])

            serializer = VehicleSerializer(vehicle)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({'errors': 'no more room available'}, status=status.HTTP_404_NOT_FOUND)


class VehicleExitView(APIView):
    def put(self, request, vehicle_id=None):

        vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
        if vehicle.space == None:
            return Response({'error': 'vehicle not found'}, status=status.HTTP_404_NOT_FOUND)

        pricing = Pricing.objects.last()
        amount_paid = pricing.calculate_fee(vehicle)

        vehicle.amount_paid = amount_paid
        vehicle.paid_at = datetime.now(timezone.utc)
        vehicle.space = None

        vehicle.save()
        serializer = VehicleSerializer(vehicle)
        return Response(serializer.data)