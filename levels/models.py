from django.db.models import Model, CharField, IntegerField
from django.db.models.deletion import CASCADE
from django.db.models.enums import TextChoices
from django.db.models.fields.related import ForeignKey


class Level(Model):
    name = CharField(max_length=255)
    fill_priority = IntegerField()


class SpaceType(TextChoices):
    CAR = 'car', 'car'
    MOTORCYCLE = 'motorcycle', 'mototcycle'


class Space(Model):
    variety = CharField(max_length=255, choices=SpaceType.choices)
    level = ForeignKey(Level, on_delete=CASCADE)
