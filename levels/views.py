from rest_framework import authentication, permissions, serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from .serializers import LevelSerializer
from .models import Level, Space, SpaceType
from utils.permissions import OnlyAdminWrite

class LevelView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [OnlyAdminWrite]

    def post(self, request):
        serializer = LevelSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializers.Serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        level = Level.objects.create(
            fill_priority=request.data['fill_priority'],
            name=request.data['name'],
        )

        for _ in range(request.data['motorcycle_spaces']):
            Space.objects.create(level=level, variety=SpaceType.MOTORCYCLE)


        for _ in range(request.data['car_spaces']):
            Space.objects.create(level=level, variety=SpaceType.CAR)
    
        serializer = LevelSerializer(level)
        
        return Response(serializer.data, status=status.HTTP_201_CREATED)


    def get(self, request):
        queryset = Level.objects.all()
        serializer = LevelSerializer(queryset, many=True)
        return Response(serializer.data)

