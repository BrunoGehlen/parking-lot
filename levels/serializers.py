from vehicles.models import Vehicle
from rest_framework import serializers
from rest_framework.serializers import Serializer
from .models import SpaceType


class LevelSerializer(Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    fill_priority = serializers.IntegerField()
    car_spaces = serializers.IntegerField(write_only=True)
    motorcycle_spaces = serializers.IntegerField(write_only=True)

    def get_available_spaces_amount(self, level):
        avaliable_motorcycle_spaces = len(level.space_set.filter(
            variety=SpaceType.MOTORCYCLE, vehicle=None
        ))
        avaliable_car_spaces = len(level.space_set.filter(
            variety=SpaceType.CAR, vehicle=None
        ))

        return {
            "available_motorcycle_spaces": avaliable_motorcycle_spaces, 
            "available_car_spaces": avaliable_car_spaces 
        }

    available_spaces = serializers.SerializerMethodField(
        'get_available_spaces_amount', read_only=True
    )


class SpaceSerializer(Serializer):
    id = serializers.IntegerField(read_only=True)
    variety = serializers.CharField()
    level_name = serializers.SerializerMethodField('get_floor')

    def get_floor(self, space):
        return space.level.name
